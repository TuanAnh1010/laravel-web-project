@extends ('layouts.layout')

@section ('content')
    <div class="content text-light">
        <div class="row">
            <div class="col-sm-8">
                <h2><center>Thông tin thanh toán</center></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 text-light">Họ và tên:</div>
            <div class="col-sm-6">{{$name}}</div>
        </div>
        <div class="row">
            <div class="col-sm-6 text-light">Tàu:</div>
            <div class="col-sm-6">{{$train}}</div>
        </div>
        <div class="row">
            <div class="col-sm-6 text-light">Nơi đi:</div>
            <div class="col-sm-6">{{$arrival}}</div>
        </div>
        <div class="row">
            <div class="col-sm-6 text-light">Nơi đến:</div>
            <div class="col-sm-6">{{$destination}}</div>
        </div>
        <div class="row">
            <div class="col-sm-6 text-light">Thời gian đi:</div>
            <div class="col-sm-6">{{$arrivalTime}}</div>
        </div>
        <div class="row">
            <div class="col-sm-6 text-light">Thời gian đến:</div>
            <div class="col-sm-6">{{$destinationTime}}</div>
        </div>
        <div class="row">
            <div class="col-sm-6 text-light">Thành tiền:</div>
            <div class="col-sm-6">{{$price}} VND</div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-6">
                        <a class="btn btn-primary pull-right" href="/customer-form" role="button">Quay lại</a>
                    </div>
                    <div class="col-sm-6">
                        <a class="btn btn-primary" href="/" role="button">Hoàn tất</a>
                    </div>
                    <div class="col-sm-6"></div>
                </div>
            </div>
        </div>
    </div>
@endsection
