<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\customerModel;
use App\tripModel;
use App\ticketModel;
use App\receiptModel;

class customerController extends Controller
{
    //Validate Customer Data
    public function validateForm(Request $request){
        $validator = Validator::make($request->all(),
            [
                'name'=>'required',
                'phone'=>'required|numeric',
                'email'=>'e-mail|nullable',
            ],
            [
                'required' => ':attribute không được để trống',
                'numeric' => ':attribute chỉ được điền số',
                'e-mail' => ':attribute không có dạng email',
            ]);

        if ($validator->fails())
        {
            return View('pages.customerForm')->withErrors($validator);
        }
        else
        {
            $customer = new customerModel;

            $customer->name = $request->name;
            $customer->phone = $request->phone;
            $customer->email = $request->email;

            $customer->save();

            $tripID = $request->session()->get('tripID');
            $trip = tripModel::find($tripID);
            $ticketID = $trip->tickets->ticketID;

            $receipt = new receiptModel;

            $receipt->ticketID = $ticketID;
            $receipt->customerID = $customer->customerID;

            $receipt->save();
            
            return redirect('receipt-form');
        }
        
    }
}
