<html lang="vi">
    <head>
        <title>Train ticket booking</title>
        @include('includes.head')
    </head>
    <body>
    	<div class="body">
    	<div id="header">
    		@include('includes.header')
    	</div>
        <div class="bg" id="main">
            <div class="container">
            <div class="form-container">
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                        @yield('content')
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
            </div>
        </div>
	    <div id="footer">
	    	<div class="row">
                <div class="col-sm-3"></div>
                <div class="col-sm-6">
                    <h6 class="text-center">
                    	Tổng công ty Đường sắt Việt Nam. Số 118 Lê Duẩn, Hoàn Kiếm, Hà Nội.
                	</h6>
                    <h6 class="text-center">Điện thoại: 19006469. Email: dsvn@vr.com.vn.</h6>
                    <h6 class="text-center">
                    	Giấy chứng nhận ĐKKD số 113642 theo QĐ thành lập số 973/QĐ-TTg ngày 25/06/2010 của Thủ tướng Chính phủ.
                    </h6>
                    <h6 class="text-center">
                    	Mã số doanh nghiệp: 0100105052, đăng ký lần đầu ngày 26/07/2010, đăng ký thay đổi lần 4 ngày 27/06/2014 tại Sở KHĐT Thành phố Hà Nội.
                    </h6>
                </div>
                <div class="col-sm-3"></div>
            </div>
        </div>
    </body>
</html>
