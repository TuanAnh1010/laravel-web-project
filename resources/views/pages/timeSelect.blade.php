@extends ('layouts.layout')

@section ('content')
 <div class="content">
    <div class="row">
        <div class="col-sm-8">
            <h2 class="text-light"><center>Chọn chuyến đi</center></h2>
        </div>
    </div>
        <div class="row">
                <div class="col-sm-8">
                <form class="form-horizontal" id="timeSelectForm" method="post" action="{{url('time-select-form-handle')}}">
                    {{ csrf_field() }}
                    @foreach ($trip as $tr)
                        <div class="form-check">
                        <input type="radio" class="form-check-input" id="tripradio" name="tripID" value={{$tr->tripID}}>
                        <label class="form-check-label text-light" for="tripradio">    
                                tàu {{$tr->train}}: đi từ {{$tr->arrivalStation->stationName}} to {{$tr->destinationStation->stationName}}: từ {{$tr->arrivalTime}} đến {{$tr->destinationTime}}
                        </label>
                    </div>
                @endforeach
                @if ($error != '')
                    <div class="is-danger">
                        <p>{{$error}}</p>
                    </div>
                @endif
                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="row">
                            <div class="col-sm-6">
                                <a class="btn btn-primary pull-right" href="/" role="button">Quay lại</a>
                            </div>
                            <div class="col-sm-6">
                                <input type="submit" class="btn btn-info btn-sm" value="Xác nhận" id="submit button">
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
