<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class receiptModel extends Model
{
    //
    public $table = 'receipt';
    public $timestamp = false;

    public function customer(){
    	return hasOne('App\tripModel','customerID','customerID');
    }

    public function ticket(){
    	return hasOne('App\ticketModel','ticketID','ticketID');
    }
}
