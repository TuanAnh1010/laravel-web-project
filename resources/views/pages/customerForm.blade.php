@extends ('layouts.layout')

@section ('content')
    <div class="content">
        <div class="row">
            <div class="col-sm-8">
                <h2 class="text-light"><center>Thông tin khách hàng</center></h2>
            </div>
        </div>
        <form id="customerForm" method="post" action="{{url('customer-form-handle')}}">
            {{ csrf_field() }}
            <!-- name form -->
            <div class="row">
                <div class="col-sm-8">
                    <div class="form-group">
                        <label for="name" class="text-light">Họ và tên (bắt buộc): </label>
                        <input type="text" class="form-control" id="name" placeholder="Tên" name="name">
                        @if ($errors->has('name'))
                            @foreach($errors->get('name') as $element)
                                <p class="help is-danger">{{ $element }}</p>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
            
            <!-- email form -->    
            <div class="row">
                <div class="col-sm-8">
                    <div class="form-group">
                        <label for="email" class="text-light">Email: </label>
                        <input type="email" class="form-control" id="email" placeholder="email" name="email">
                        @if ($errors->has('email'))
                            @foreach($errors->get('email') as $element)
                                <p class="help is-danger">{{ $element }}</p>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
            

            <!-- phone form -->
            <div class="row">
                <div class="col-sm-8">
                    <div class="form-group">
                        <label for="phone" class="text-light">Số điện thoại (bắt buộc): </label>
                        <input type="text" class="form-control" id="phone" placeholder="Số điện thoại" name="phone">
                        @if ($errors->has('phone'))
                            @foreach($errors->get('phone') as $element)
                                <p class="help is-danger">{{ $element }}</p>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>

            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
            <div class="row">
                <div class="col-sm-8">
                    <div class="row">
                        <div class="col-sm-6">
                            <a class="btn btn-primary pull-right" href="/time-picking-form" role="button">Quay lại</a>
                        </div>
                        <div class="col-sm-6">
                            <input type="submit" class="btn btn-info btn-sm" value="Xác nhận" id="submit button">
                        </div>
                    </div>
                </div>
            </div>
            
        </form>
    </div>
@endsection
