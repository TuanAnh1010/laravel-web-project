<?php

namespace App\Http\Controllers;

use App\stationModel;
use Illuminate\Http\Request;

class bookingFormController extends Controller
{
    public function getStation(){
        $station = stationModel::all();
        return $station;
    }

    public function checking(Request $request){
        $arrival = $request->input('arrival');
        $destination = $request->input('destination');
        $date = $request->get('date');
        $request->session()->put('arrival',$arrival);
        $request->session()->put('destination',$destination);
        $request->session()->save();
        if ($arrival == $destination){
            $error = 'Nơi đi và nơi đến phải khác nhau';
            $station = $this->getStation();
            return View('pages.tripBookingForm')->with('error',$error)->with('station',$station);
        }
        else {
            return redirect('time-picking-form')->with('request',$request);
        }
    }
}
