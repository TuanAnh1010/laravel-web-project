<?php

namespace App\Http\Controllers;

use App\receiptModel;
use App\tripModel;

use Illuminate\Http\Request;

class receiptController extends Controller
{
    //
    public function getTrip(Request $request){
    	$tripID = $request->session()->get('tripID');
    	$trip = tripModel::find($tripID);
    	return $trip;
    }

    public function getPrice(Request $request){
        $trip = $this->getTrip($request);
        $price = $trip->tickets->price;
        return $price;
    }

    public function getName(Request $request){
        $trip = $this->getTrip($request);
        $ticket = $trip->tickets;
        $name = $ticket->customers->first()->name;
        return $name;
    }

    


}
