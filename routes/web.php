<?php

use App\BookingDataModel;
use Illuminate\Http\Request;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', ['uses'=>'pagesController@setBookingForm']);
Route::get('booking-form',['uses'=>'pagesController@setBookingForm']);
Route::get('time-picking-form',['uses'=>'pagesController@setTripSelect']);
Route::get('customer-form',['uses'=>'pagesController@setCustomerForm']);
Route::get('receipt-form',['uses'=>'pagesController@setReceiptsForm']);

Route::post('booking-form-handle',['uses'=>'bookingFormController@checking']);
Route::post('time-select-form-handle',['uses'=>'tripBookingController@storing']);
Route::post('customer-form-handle',['uses'=>'customerController@validateForm']);
Route::post('receipt-form-handle',['uses'=>'receiptController@confirmForm']);

