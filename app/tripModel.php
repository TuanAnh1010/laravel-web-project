<?php

namespace App;

use Illuminate\Database\Eloquent;
use App\stationModel;

class tripModel extends Eloquent\Model
{
    //
    protected $table = 'trip';
    protected $primaryKey = 'tripID';
    public $timestamp = false;

    public function arrivalStation(){
        return $this->belongsTo('App\stationModel','arrival_stationID','stationID');
    }

    public function tickets(){
        return $this->hasOne('App\ticketModel','tripID','tripID');
    }

    protected function destinationStation(){
        return $this->belongsTo('App\stationModel','destination_stationID','stationID');
    }

    protected function receipts(){
        return $this->hasManyThrough('App\receiptModel','App\ticketModel','tripID','ticketID','tripID','ticketID');
    }
}

