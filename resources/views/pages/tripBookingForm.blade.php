@extends('layouts.layout')

@section('content')

    <div class="content">
        <div class="row">
            <div class="col-sm-8">
                <h2 class="text-light"><center>Bắt đầu đặt chỗ</center></h2>
            </div>
        </div>
        <form class="form-horizontal" id="initialForm" method="post" action="{{url('booking-form-handle')}}">
        {{ csrf_field() }}

            <div class="row">
                    <!-- dropdown arrival list -->
                <div class="dropdown form-group col-sm-8">
                    <label for="arrival" class="text-light">Nơi đi</label>
                    <select class="form-control" id="arrival" name="arrival">
                        @foreach ($station as $arvst)
                            echo <option value={{$arvst->stationName}}>{{$arvst->stationName}}</option>
                        @endforeach
                    </select>
                    <div><p class="help is-danger">{{$error}}</p></div>
                </div>
            </div>
            <div class="row">
                    <!-- dropdown destination list -->
                <div class="dropdown form-group col-sm-8">
                    <label for="destination" class="text-light">Nơi đến</label>
                    <select class="form-control" id="destination" name="destination">
                        @foreach ($station as $dstst)
                            echo <option value={{$dstst->stationName}}>{{$dstst->stationName}}</option>
                        @endforeach
                    </select>
                    <div><p class="help is-danger">{{$error}}</p></div>
                </div>
            </div>
            
            <div class="row">
                <div class="form-check-inline">
                    <label class="form-check-label text-light" for="onewayradio">
                        <input type="radio" class="form-check-input" id="onewayradio" name="optradio" checked>Một chiều
                    </label>
                </div>
                <div class="form-check-inline">
                    <label class="form-check-label text-light" for="returnradio">
                        <input type="radio" class="form-check-input" id="returnradio" name="optradio" >Khứ hồi
                    </label>
                </div>
            </div>
            
            <div class="row"t" >
                <div class="col-sm-6">
                    <input type="submit" class="btn btn-info btn-sm pull-right" value="Xác nhận" id="submit button">
                </div>   
            </div> 
        </form>
    </div>

@endsection
