<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class customerModel extends Model
{
    //
    protected $table='customer';
    protected $primaryKey='customerID';

    protected $fillable = ['name','phone','email'];
    public $timestamp = false;
    
    
}
