<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class stationModel extends Model
{
    //
    protected $table = 'station';
    protected $primaryKey = 'stationID';
    public $timestamp = false;

    public function arrivalStation(){
        return $this->hasMany('App\tripModel','stationID','arrival_stationID');
    }

    public function destinationStation(){
        return $this->hasMany('App\tripModel','stationID','destination_stationID');
    }
}
