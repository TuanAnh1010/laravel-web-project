<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ticketModel extends Model
{
    /*
     * The attribute that are mass assignable.
     *
     * @var array
     */

    protected  $table = 'ticket';
    public $timestamp = false;

    public function trips(){
    	return $this->belongTo('App\tripModel','tripID','tripID');
    }

    public function receipts(){
        return $this->belongTo('App\receiptModel','ticketID','ticketID');
    }

    public function customers(){
        return $this->hasManyThrough('App\customerModel','App\receiptModel','ticketID','customerID','ticketID','customerID');
    }

}
