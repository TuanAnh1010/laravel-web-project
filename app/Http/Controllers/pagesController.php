<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class pagesController extends Controller
{
    //
    public function setBookingForm(){
        $stationController = new bookingFormController();
        $station = $stationController->getStation();
        $error = '';
        return View('pages.tripBookingForm')->with('station',$station)->with('error',$error);
    }

    public function setTripSelect(Request $request){
        $tripController = new tripBookingController();
        $trip = $tripController->getTrip($request);
        $error = '';
        return View('pages.timeSelect')->with('trip',$trip)->with('error',$error);
    }

    public function setCustomerForm(Request $request){
        $validation = '';
        return View('pages.customerForm')->withErrors($validation);
    }

    public function setReceiptsForm(Request $request){
        $receipt = new receiptController();
        $trip = $receipt->getTrip($request);
        $name = $receipt->getName($request);
        $price = $receipt->getPrice($request);
        $arrival = $trip->arrivalStation->stationName;
        $destination = $trip->destinationStation->stationName;
        $arrivalTime = $trip->arrivalTime;
        $destinationTime = $trip->destinationTime;
        $train = $trip->train;
        return View('pages.confirmTransaction')
                    ->with('name',$name)
                    ->with('train',$train)
                    ->with('arrival',$arrival)
                    ->with('destination',$destination)
                    ->with('arrivalTime',$arrivalTime)
                    ->with('destinationTime',$destinationTime)
                    ->with('price',$price);
    }

}
