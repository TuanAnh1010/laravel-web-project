<?php

namespace App\Http\Controllers;

use App\tripModel;
use App\stationModel;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Illuminate\Support\Facades\DB;

class tripBookingController extends Controller
{
    //checking date time and giving train id
    public function getTrip(Request $request){

        $arrival = $request->session()->get('arrival');
        $destination = $request->session()->get('destination');

        $arrID = stationModel::where('stationName',$arrival)->first();
        $dstID = stationModel::where('stationName',$destination)->first();

        $trip = tripModel::where('arrival_stationID',$arrID->stationID)
            ->where('destination_stationID',$dstID->stationID)
            ->get();

        return $trip;
    }

    public function storing(Request $request){
        $tripID = $request->input('tripID');
        if ($tripID == ''){
            $error = "Bạn phải chọn chuyến đi";
            return View('pages.timeSelect')->with('error',$error)->with('trip',$this->getTrip($request));
        }
        else {
        $request->session()->put('tripID',$tripID);
        $request->session()->save();
        return redirect('customer-form')->with('request',$request);
    }
    }
}


